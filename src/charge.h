/*
 * charge.h
 *
 *  Created on: 18-04-2015
 *      Author: ktc
 */


#include <cuda_runtime.h>



#define K_CONST 1.0f
#define VALUE_RANGE 200
#define PRETTY_CONST 100
#define SPEED 1
typedef struct {
	int chargesCnt;
	float2* chargeLocations;
	float* chargeValues;
	int2* direction;
} chargesInfo;


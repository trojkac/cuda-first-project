/*
 * Copyright 1993-2014 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

// Utilities and system includes

#include <helper_cuda.h>
#include <stdlib.h>
#include "charge.h"
#include <math.h>
#include <curand.h>
#include <curand_kernel.h>
// clamp x to range [a, b]

int offset = 0;

__device__ float clamp(float x, float a, float b)
{
    return max(a, min(b, x));
}

__device__ int clamp(int x, int a, int b)
{
    return max(a, min(b, x));
}

// convert floating point rgb color to 8-bit integer
__device__ int rgbToInt(float r, float g, float b)
{
    r = clamp(r, 0.0f, 255.0f);
    g = clamp(g, 0.0f, 255.0f);
    b = clamp(b, 0.0f, 255.0f);
    return (int(b)<<16) | (int(g)<<8) | int(r);
}

__global__ void
moveChargesRandom(int imgw, float2* loc, int2* dir, int cnt,int offset)
{
	int tx = threadIdx.x;
    int ty = threadIdx.y;
    int bw = blockDim.x;
    int bh = blockDim.y;
    int tid = (blockIdx.x + blockIdx.y*gridDim.x)*bw*bh + ty*bw + tx;
  	curandState_t state;
 	curand_init(0, tid, offset, &state);

 	if(curand(&state)%7 == 0)dir[tid].x *= -1;
 	if(curand(&state)%8 == 0)dir[tid].y *= -1;

	int lx = loc[tid].x + SPEED * dir[tid].x;
	int ly = loc[tid].y + SPEED * dir[tid].y;

	loc[tid].x = lx >= 0 ? lx < imgw ? lx : 2*imgw - lx : - lx;
	loc[tid].y = ly >= 0 ? ly < imgw ? ly : 2*imgw - ly : - ly;
}
__global__ void
moveCharges(float2* loc, float* val, int cnt)
{
		int tx = threadIdx.x;
	    int ty = threadIdx.y;
	    int bw = blockDim.x;
	    int bh = blockDim.y;
	    int tid = (blockIdx.x + blockIdx.y*gridDim.x)*bw*bh + ty*bw + tx;
	    int i;


	    float3 point = {0,0};
	    float q = val[tid];
	    float x = loc[tid].x;
	    float y = loc[tid].y;
	    __syncthreads();

	    for(i = 0;i < cnt;i++)
	    {
	    	if(i == tid)continue;
	    	float3 tmp;
	    	float2 location = loc[i];
	    	float dx = x-location.x,dy = y-location.y;
	    	if(dx == 0 && dy == 0)continue;
	    	float r2 = (float)(dx*dx+dy*dy);
	    	tmp.z = K_CONST*val[i]*q/r2;

	    	float r = sqrtf(r2);

	    	tmp.x = dx/r*tmp.z;
	    	tmp.y = dy/r*tmp.z;
	    	dx = abs(dx);
	    	dy = abs(dy);
	    	point.x += clamp(tmp.x,-dx,dx);
	    	point.y += clamp(tmp.y,-dy,dy);
	    }
	    __syncthreads();
	    loc[tid].x += clamp(point.x,-10.0f,10.0f);
	    loc[tid].y += clamp(point.y,-10.0f,10.0f);

}
__global__ void
computeColors(unsigned int *g_odata, int imgw,float2* loc,float* val, int cnt)//, chargesInfo *charges)
{


    int tx = threadIdx.x;
    int ty = threadIdx.y;
    int bw = blockDim.x;
    int bh = blockDim.y;
    int x = blockIdx.x*bw + tx;
    int y = blockIdx.y*bh + ty;
    int i;


    float3 point = {0,0};
    float scale_max(K_CONST * VALUE_RANGE / 2 / PRETTY_CONST);
    for(i = 0;i < cnt;i++)
    {
    	float3 tmp;
    	float2 location = loc[i];
    	float dx = x-location.x,dy = y-location.y;

    	float r2 = (float)(dx*dx+dy*dy);
    	tmp.z = K_CONST*val[i]/r2;

    	if(abs(tmp.z) < scale_max/255) continue;
    	float r = sqrtf(r2);

    	tmp.x = dx/r*tmp.z;
    	tmp.y = dy/r*tmp.z;

    	point.x += tmp.x;
    	point.y += tmp.y;
    }

    float E = sqrtf(point.x*point.x+point.y*point.y);
    float intense = 255 * E/scale_max;
    g_odata[y*imgw+x] = rgbToInt(intense, intense - 255, 255-intense);


}

extern "C" void
launch_cudaProcess(dim3 grid, dim3 block, int sbytes,
                   unsigned int *g_odata,
                   chargesInfo* charges,
                   int imgw, bool random_movement)
{


//	int2loc;
//	loc.x=256;
//	loc.y=256;
//	float val=10;


	float2* d_loc;
	float* d_val;
	int2* d_dir;




    checkCudaErrors(cudaMalloc((void **) &d_loc,sizeof(float2)*(charges->chargesCnt)));
    checkCudaErrors(cudaMalloc((void **) &d_val,sizeof(float)*charges->chargesCnt));
    checkCudaErrors(cudaMalloc((void**) &d_dir,sizeof(int2)*charges->chargesCnt));

    checkCudaErrors(cudaMemcpy(d_val,
    							charges->chargeValues,
    							sizeof(float)*(charges->chargesCnt),
    							cudaMemcpyHostToDevice));

    checkCudaErrors(cudaMemcpy(d_loc,
        						charges->chargeLocations,
        						sizeof(float2)*charges->chargesCnt,
        						cudaMemcpyHostToDevice));
    //      setup execution parameters
    dim3 th(27,1,1);
    dim3 gr(1,1,1);


    if(random_movement)
    {

        checkCudaErrors(cudaMemcpy(d_dir,
            						charges->direction,
            						sizeof(int2)*charges->chargesCnt,
            						cudaMemcpyHostToDevice));

        moveChargesRandom<<< gr, th >>>(imgw,d_loc,d_dir,charges->chargesCnt,offset++);
    }

    else
    {
    	moveCharges<<<gr,th>>>(d_loc,d_val,charges->chargesCnt);
    }
    computeColors<<< grid, block, sbytes >>>(g_odata, imgw,d_loc,d_val, charges->chargesCnt);

    checkCudaErrors(cudaMemcpy(charges->chargeLocations,
            					d_loc,
            					sizeof(float2)*charges->chargesCnt,
            					cudaMemcpyDeviceToHost));
    checkCudaErrors(cudaMemcpy(charges->direction,
              					d_dir,
              					sizeof(int2)*charges->chargesCnt,
              					cudaMemcpyDeviceToHost));
    getLastCudaError("Kernel execution failed");

    if(random_movement)
    	checkCudaErrors(cudaFree(d_dir));

    checkCudaErrors(cudaFree(d_loc));
    checkCudaErrors(cudaFree(d_val));
}
